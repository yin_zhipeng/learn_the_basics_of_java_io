package nio.buffer;

import org.junit.Test;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Buffer基本使用例子
 */
public class BufferForExample {
    /**
     * 缓冲区分片
     */
    @Test
    public void test1() throws Exception{
        //10个byte大小的byte缓冲区
        ByteBuffer buffer = ByteBuffer.allocate(10);
        //给这缓冲区中添加数据
        for(int i = 0;i<buffer.capacity();i++){
            buffer.put((byte)i);
        }
        System.out.println("===========================输出整个缓冲区数据=============================");
        //重置指针
        buffer.position(0);
        buffer.limit(buffer.capacity());
        while(buffer.remaining()>0){
            System.out.print(buffer.get()+" ");
        }
        System.out.println();
        //创建子缓冲区
        buffer.position(3);//起始下标3
        buffer.limit(7);//界限为7
        ByteBuffer slice = buffer.slice();//子缓冲区3~7（不含7）

        //改变子缓冲区内容,子缓冲区每个数据都*10
        System.out.println("===========================改变子缓冲区内容,子缓冲区每个数据都*10:(3~7（不含7）)=============================");
        for(int i = 0;i < slice.capacity();i++){
            byte b = slice.get(i);
            b *= 10;
            slice.put(i,b);
        }

        //重置指针
        buffer.position(0);
        buffer.limit(buffer.capacity());

        //输出整个缓冲区数据,remaining()方法返回limit - position的值，也就是剩余可访问元素个数
        System.out.println("===========================输出整个缓冲区数据=============================");
        while(buffer.remaining()>0){
            System.out.print(buffer.get()+" ");
        }
    }
    /**
     * 只读缓冲区
     */
    @Test
    public void test2() throws Exception{
        //10个byte大小的byte缓冲区
        ByteBuffer buffer = ByteBuffer.allocate(10);
        //给这缓冲区中添加数据
        for(int i = 0;i<buffer.capacity();i++){
            buffer.put((byte)i);
        }

        System.out.println("===========================输出整个缓冲区数据=============================");
        System.out.println(Arrays.toString(buffer.array()));

        //创建只读缓冲区
        ByteBuffer readOnlyBuffer = buffer.asReadOnlyBuffer();
        //改原缓冲区
        System.out.println("===========================原缓冲区下标为3位置的值改为7777=============================");
        buffer.put(3,(byte)7777);

        System.out.println("===========================输出只读缓冲区内容=============================");
        //给这缓冲区中添加数据
        for(int i = 0;i<buffer.capacity();i++){
            System.out.print(readOnlyBuffer.get(i)+" ");
        }
    }
    /**
     * 内存映射文件I/O
     */
    static private final int start = 0;
    static private final int size = 1024;
    @Test
    public void test3() throws Exception{
        RandomAccessFile raf = new RandomAccessFile("E:/1.txt","rw");
        FileChannel fc = raf.getChannel();
        //内存映射字节缓存MappedByteBuffer
        MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_WRITE, start, size);
        mbb.put(0,(byte)97);
        mbb.put(1023,(byte)122);
        raf.close();
    }
}
package nio.path;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class NIOPathTest {
    @Test
    public void test() throws Exception{
        Path path = Paths.get("E:/1.txt");
        //指向目录：E:\projects
        Path projects = Paths.get("E:/", "projects");
        //指向文件：E:\projects\1.txt
        Path path1 = Paths.get("E:/", "projects/1.txt");

        //Path.normalize()标准化路径
        String originalPath = "E:/projects/../aa-project";
        Path path2 = Paths.get(originalPath);
        System.out.println(path2);//E:\projects\..\aa-project
        Path normalize = path2.normalize();
        System.out.println(normalize);//E:\aa-project

        Path rootPath = Paths.get("d:\\test");//根目录
        String fileToFind = File.separator + "001.txt";//要查找的文件
        try {
            Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws
                        IOException {
                    String fileString = file.toAbsolutePath().toString();
                    //System.out.println("pathString = " + fileString);
                    if(fileString.endsWith(fileToFind)){
                        System.out.println("file found at path: " + file.toAbsolutePath());
                        return FileVisitResult.TERMINATE;
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}

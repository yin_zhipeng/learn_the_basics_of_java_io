package nio.file_lock;

import org.junit.Test;

import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileLockTest {
    @Test
    public void test1() throws Exception{
        String input = "FileLockTest要写的内容";
        System.out.println("input=====>>>>"+input);

        //通过字节数组，搞缓冲区，allocate是指定大小
        ByteBuffer buffer = ByteBuffer.wrap(input.getBytes(StandardCharsets.UTF_8));

        //获取一个Path对象
        String filePath = "E:/1.txt";
        Path path = Paths.get(filePath);

        //打开通道，文件路径为path，写操作，追加模式
        FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.WRITE, StandardOpenOption.APPEND);

        //定位到通道末尾可插入位置
        long size = fileChannel.size();//当前通道中，文件的大小
        fileChannel.position(size-1);//我们要追加内容到通道后面，正好是通道当前已有内容的后面

        //加锁
        FileLock lock = fileChannel.lock();
        System.out.println("是否是共享锁："+lock.isShared());
        fileChannel.write(buffer);//将缓冲区内容写入
        fileChannel.close();//关闭通道
        System.out.println("写入完成！！！");
    }
}

package nio.channel;
import org.junit.Test;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class DatagramChannelTest {
    //发送报文
    @Test
    public void sendDatagram() throws Exception{
        //打开 DatagramChannel
        DatagramChannel sendChannel = DatagramChannel.open();
        //发送
        while(true){
            ByteBuffer buffer = ByteBuffer.wrap("我是要发送的UPD报文".getBytes(StandardCharsets.UTF_8));
            sendChannel.send(buffer,new InetSocketAddress("127.0.0.2",9999));
            System.out.println("发送完成");
            Thread.sleep(1000);
        }
    }
    //接收报文
    @Test
    public void receiveDatagram() throws Exception{
        //打开 DatagramChannel
        DatagramChannel receiveChannel = DatagramChannel.open();
        InetSocketAddress receiveAddress = new InetSocketAddress(9999);
        //绑定,和ServerSocketChannel不同，这个可以直接绑定
        receiveChannel.bind(receiveAddress);
        //buffer
        ByteBuffer receiveBuffer = ByteBuffer.allocate(1024);
        //接收
        while(true){
            receiveBuffer.clear();//先清除，免得缓冲区有莫名其妙的东西
            //receive()接收UDP包，SocketAddress可以获得发包的ip、端口等信息，用toString查看
            SocketAddress socketAddress = receiveChannel.receive(receiveBuffer);
            System.out.println(socketAddress.toString());

            receiveBuffer.flip();//读写转换
            //Charset.forName("UTF-8").decode(str)，对str进行UTF-8的编码
            System.out.println(Charset.forName("UTF-8").decode(receiveBuffer));
        }
    }

}

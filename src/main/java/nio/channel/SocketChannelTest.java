package nio.channel;


import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class SocketChannelTest {
    public static void main(String[] args) {
        SocketChannelTest socketChannelTest = new SocketChannelTest();
        try {
            socketChannelTest.test1();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
    public void test1()throws Exception{
        //1.创建SocketChannel
        //方式1：通过SocketChannel.open创建SocketChannle，InetSocketAddress是工具类，指定ip和端口
        SocketChannel sc = SocketChannel.open(new InetSocketAddress( 8080));

        //2.校验
        System.out.println("测试SocketChannel是否为open状态"+sc.isOpen());
        System.out.println("试SocketChannel是否被连接"+sc.isConnected());
        System.out.println("测试SocketChannel是否正在进行连接"+sc.isConnectionPending());
        System.out.println("校验正在进行套接字连接的SocketChannel是否已经完成连接"+sc.finishConnect());
        //3. 非阻塞模式
        sc.configureBlocking(false);
        //4. 缓冲区
        ByteBuffer buffer = ByteBuffer.allocate(16);
        int byteCount = 0;
        while(sc.isConnected()){
            if((byteCount=sc.read(buffer))!=-1){
                System.out.println(new String(buffer.array(),0,byteCount));
            }else{
                sc.close();
            }
        }
    }
}

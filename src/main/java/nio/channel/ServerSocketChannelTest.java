package nio.channel;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

/**
 * ServerSocketChannel测试
 */
public class ServerSocketChannelTest {
    public static void main(String[] args) {
        ServerSocketChannelTest serverSocketChannelTest = new ServerSocketChannelTest();
        try {
            serverSocketChannelTest.test();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     *  测试ServerSocketChannel
     */
    public void test() throws Exception {
        //定义端口号
        int port = 8080;
        //buffer，玩通道，必须要缓冲区
        ByteBuffer buffer = ByteBuffer.wrap("准备写的内容alkdsjfl234324k123sajdf".getBytes(StandardCharsets.UTF_8));
        //ServerSocketChannel.open()打开通道
        ServerSocketChannel ssc = ServerSocketChannel.open();
        //绑定ip和端口号，通道需要绑定对等socket对象，但通道本身没有绑定方法,需要借助对等socket对象的绑定方法，InetSocketAddress是一个提供本地ip+端口的工具类
        ssc.socket().bind(new InetSocketAddress(port));
        //设置为非阻塞模式,configureBlocking(boolean b)方法，b是true表示阻塞，false表示非阻塞
        ssc.configureBlocking(false);
        //开始监听连接
        while(true){
            System.out.println("Waiting for connections");
            SocketChannel sc = ssc.accept();//ServerSocketChannel上调用accept()方法则会返回SocketChannel类型对象，这个对象可以在非阻塞模式下运行
            if(sc == null){
                System.out.println("null,没有连接传入");
                Thread.sleep(2000);
            }else{//有链接
                System.out.println("Incoming connection from:"+sc.socket().getRemoteSocketAddress());
                buffer.rewind();//指针置为0
                sc.write(buffer);//通过通道将缓冲区内容写入
                sc.shutdownOutput();
                sc.close();
            }
        }
    }
}

package nio.channel;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.charset.StandardCharsets;
import java.util.AbstractList;

/**
 * FileChannel测试
 */
public class FileChannelTest {
    public static void main(String[] args) {
        FileChannelTest fileChannelTest = new FileChannelTest();
        try {
//            fileChannelTest.test();
//            fileChannelTest.test2();
//            fileChannelTest.test3();
            fileChannelTest.test4();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *  FileChannel将buffer中内容写到文件中
     */
    public void test()throws IOException{
        System.out.println("===========================================数据写入开始================================");
        //打开fileChannel
        RandomAccessFile aFile = new RandomAccessFile("E:/1.txt", "rw");//读写模式打开文件
        FileChannel channel = aFile.getChannel();//获取通道
        //创建Buffer
        ByteBuffer buf = ByteBuffer.allocate(1024);//用字节缓存区

        //要写入的内容
        String str = new String("贼爽，这是NIO中FileChannel要写入的内容1123444alsdkfjlaksdf");
        //清空一下，保险点，万一里面有东西呢
        buf.clear();
        //写入
        buf.put(str.getBytes(StandardCharsets.UTF_8));
        //读写转换
        buf.flip();
        System.out.println("buf.flip():切换读写模式,上面都是将数据读入到缓冲区，这里转换，就可以直接写，而不用再建一个输出流");
        //如果Buffer中有内容，就一直写
        while(buf.hasRemaining()){//如果有内容可读
            channel.write(buf);//读写由channel调用Buffer完成，不是channel完成
        }
        buf.clear();//清理缓冲区
        channel.close();//关通道
        aFile.close();//关流

        System.out.println("===========================================数据写入完成================================");
    }

    /**
     * FileChannel读取数据到buffer中
     */
    public void test2()throws IOException {
        System.out.println("===========================================数据读取开始================================");
        //打开FileChannel
        RandomAccessFile aFile = new RandomAccessFile("E:/1.txt", "rw");//读写模式打开文件
        FileChannel channel = aFile.getChannel();//获取通道

        //创建Buffer
        ByteBuffer buf = ByteBuffer.allocate(1024);//用字节缓存区

        //读取数据到buffer中,和基本IO基本一致
        int byteCount = 0;//读了几个字节
        while((byteCount = channel.read(buf))!=-1){
            System.out.print("读取内容："+new String(buf.array(),0,byteCount));
            buf.flip();//切换读写模式------》上面都是将数据读入到缓冲区，这里转换，就可以直接写，而不用再建一个输出流
            System.out.println("buf.flip():切换读写模式,上面都是将数据读入到缓冲区，这里转换，就可以直接写，而不用再建一个输出流");
            while(buf.hasRemaining()){//如果有内容可读
                System.out.print(buf.get());
            }
            buf.clear();
        }
        channel.close();
        aFile.close();
        System.out.println("========读取数据完成=======");

    }
    public void test3()throws IOException{
        //fromChannel，被拷贝
        RandomAccessFile aFile = new RandomAccessFile("E:/1.txt", "rw");//读写模式打开文件
        FileChannel fromChannel = aFile.getChannel();//获取通道

        //toChannel，要拷贝东西过来
        RandomAccessFile bFile = new RandomAccessFile("E:/2.txt", "rw");//读写模式打开文件
        FileChannel toChannel = bFile.getChannel();//获取通道

        long position = 0;//起始位置
        long count = fromChannel.size();//实例所关联文件大小
        //a.transferFrom(b,...,...);将b的搞到a中，transferFrom就是将东西搞到自己这里
        toChannel.transferFrom(fromChannel,position,count);
        aFile.close();
        bFile.close();
        System.out.println("E:/1.txt文件内容到E:/2.txt拷贝完成！！！！");
    }
    public void test4()throws IOException{
        //fromChannel，被拷贝
        RandomAccessFile aFile = new RandomAccessFile("E:/1.txt", "rw");//读写模式打开文件
        FileChannel fromChannel = aFile.getChannel();//获取通道

        //toChannel，要拷贝东西过来
        RandomAccessFile bFile = new RandomAccessFile("E:/3.txt", "rw");//读写模式打开文件
        FileChannel toChannel = bFile.getChannel();//获取通道

        long position = 0;//起始位置
        long count = fromChannel.size();//实例所关联文件大小
        //transferTo
        fromChannel.transferTo(position,count,toChannel);
        aFile.close();
        bFile.close();
        System.out.println("E:/1.txt文件内容到E:/3.txt拷贝完成！！！！");
    }
}

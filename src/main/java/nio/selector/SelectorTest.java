package nio.selector;

import org.junit.Test;

import java.io.FileOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

public class SelectorTest {
    @Test
    public void test()throws Exception{
        //1、获取Selector选择器
        Selector selector = Selector.open();
        //2、获取通道
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //3、设置为非阻塞
        serverSocketChannel.configureBlocking(false);
        //4、绑定连接
        serverSocketChannel.bind(new InetSocketAddress(9999));
        //5、将通道注册到选择器上，并制定监听事件为："接收"事件
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        //6、查询已就绪通道操作，然后遍历集合，处理这些操作
        Set<SelectionKey> selectionKeys = selector.selectedKeys();
        //遍历集合
        Iterator<SelectionKey> iterator = selectionKeys.iterator();
        while(iterator.hasNext()){
            SelectionKey key = iterator.next();
            //判断key的状态
            if(key.isAcceptable()){//就绪新套接字连接
                //@TODO 就绪新套接字连接状态操作代码
            }else if(key.isConnectable()){
                //@TODO 就绪套接字连接完成或未完成状态操作代码
            }else if(key.isReadable()){
                //@TODO 就绪读状态操作代码
            }else if(key.isWritable()){
                //@TODO 就绪写状态操作代码
            }
            iterator.remove();//移除遍历的元素
        }
    }
    @Test
    public void server() throws Exception{
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.socket().bind(new InetSocketAddress("127.0.0.1",8000));
        ssc.configureBlocking(false);

        Selector selector = Selector.open();
        // 注册 channel，并且指定感兴趣的事件是 Accept
        ssc.register(selector,SelectionKey.OP_ACCEPT);
        //缓冲区
        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
        ByteBuffer writeBuffer = ByteBuffer.allocate(128);
        //先读点内容，然后让它写
        writeBuffer.put("received".getBytes());
        //改变读写模式
        writeBuffer.flip();

        //开始轮询
        while(true){
            int nReady = selector.select();//阻塞轮询
            //当监听到操作就绪，获取操作列表
            Set<SelectionKey> keys = selector.selectedKeys();
            //迭代器迭代
            Iterator<SelectionKey> it = keys.iterator();

            while (it.hasNext()){
                SelectionKey key = it.next();
                it.remove();
                if(key.isAcceptable()){
                    // 创建新的连接，并且把连接注册到 selector 上，而且，
                    // 声明这个 channel 只对读操作感兴趣。
                    SocketChannel socketChannel = ssc.accept();
                    socketChannel.configureBlocking(false);
                    socketChannel.register(selector, SelectionKey.OP_READ);
                    System.out.println("isAcceptable,监听到连接就绪操作");
                }else if (key.isReadable()) {
                    SocketChannel socketChannel = (SocketChannel) key.channel();
                    readBuffer.clear();
                    socketChannel.read(readBuffer);
                    readBuffer.flip();
                    System.out.println("received 监听到读就绪操作，读到内容如下: " + new String(readBuffer.array()));
                    key.interestOps(SelectionKey.OP_WRITE);
                }else if (key.isWritable()) {
                    readBuffer.rewind();
                    SocketChannel socketChannel = (SocketChannel) key.channel();
                    socketChannel.write(readBuffer);
                    key.interestOps(SelectionKey.OP_READ);
                    System.out.println("isWritable 监听到写就绪操作，将读到内容写回去");

                }
            }
        }

    }
    @Test
    public void ClientDemo() throws Exception{
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.connect(new InetSocketAddress("127.0.0.1", 8000));
        //缓冲区
        ByteBuffer writeBuffer = ByteBuffer.allocate(32);
        ByteBuffer readBuffer = ByteBuffer.allocate(32);
        //读个hello
        writeBuffer.put("hello".getBytes());
        //切换读写模式
        writeBuffer.flip();
        //疯狂读写
        while (true) {
            //重置读写指针，将position设回0，可以重读或重写Buffer中数据。limit不变，依然表示读写的界限
            writeBuffer.rewind();
            //将缓冲区内容写入,服务端会进行读就绪
            socketChannel.write(writeBuffer);
            //重置缓冲区，position置为0，limit设置为capacity，表面上Buffer被清空，实际上数据并没有清除
            readBuffer.clear();
            //将通道内容读到readBuffer，服务端执行写就绪
            socketChannel.read(readBuffer);
            readBuffer.flip();
            System.out.println("服务端写过来的东西："+new String(readBuffer.array(),0,readBuffer.limit()));
            Thread.sleep(500);
        }
    }
}

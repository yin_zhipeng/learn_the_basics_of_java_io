# basics_of_java_io
    java基础IO学习
博客：[https://blog.csdn.net/grd_java/article/details/122487026](https://blog.csdn.net/grd_java/article/details/122487026)
# bio
    BIO(同步阻塞)学习
博客：[https://blog.csdn.net/grd_java/article/details/122544049](https://blog.csdn.net/grd_java/article/details/122544049)
# nio  && nio_chat
    NIO(同步不阻塞)学习,基于NIO实现的聊天室案例
博客：[https://blog.csdn.net/grd_java/article/details/122544049](https://blog.csdn.net/grd_java/article/details/122544049)
# aio
    AIO(异步不阻塞)学习
博客：[https://blog.csdn.net/grd_java/article/details/122544049](https://blog.csdn.net/grd_java/article/details/122544049)
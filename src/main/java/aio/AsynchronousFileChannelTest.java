package aio;

import org.junit.Test;

import java.io.File;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.Future;

/**
 * AIO学习
 */
public class AsynchronousFileChannelTest {
    /**
     * 创建AsynchronousFileChannel
     */
    @Test
    public void test1() throws Exception{
        //创建Path路径
        Path path = Paths.get("E:/1.txt");
        //获取AIO通道，StandardOpenOption.READ,StandardOpenOption.WRITE表示对文件执行读和写操作
        AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(path, StandardOpenOption.READ,StandardOpenOption.WRITE);

    }

    /**
     * 通过AIO异步获取1.txt文件中内容
     * @throws Exception
     */
    @Test
    public void test2() throws Exception{
        //创建路径
        Path path = Paths.get("E:/1.txt");
        //AIO通道，读操作
        AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(path, StandardOpenOption.READ);
        //缓冲区，和位置0
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        long position = 0;
        //调用AIO通道的read()方法，获取Future
        Future<Integer> operation = asynchronousFileChannel.read(buffer, position);
        //因为AIO是异步的，所以我这里要人为阻塞住它。isDone()方法，直到返回true后，才可以继续运行
        while(!operation.isDone());
        //切换读写模式
        buffer.flip();
        //因为我们创建的AIO只能读，所以我们人为遍历一下缓冲区
        byte[] data = new byte[buffer.limit()];//用来保存缓冲区内容
        buffer.get(data);
        //输出
        System.out.println(new String(data));
        //清空缓冲区
        buffer.clear();
        //关闭通道
        asynchronousFileChannel.close();
    }
    /**
     * 通过AIO异步获取1.txt文件中内容,利用CompletionHandler
     */
    @Test
    public void test3() throws Exception{
        //路径
        Path path = Paths.get("E:/1.txt");
        //AIO通道
        AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(path, StandardOpenOption.READ);
        //缓冲区和位置0
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        long position = 0;
        //read(字节要传送到的缓冲区,开始传输的文件位置(>=0),要附加到I/O操作的对象(可以为空),用于消费结果的处理程序)
        //其中第三个参数是附加的对象，attachment,会作为completed()的参数
        asynchronousFileChannel.read(buffer, position, buffer,new CompletionHandler<Integer,ByteBuffer>() {
            /**
             * 执行操作
             * @param result 读取了多少字节
             * @param attachment read()方法的第三个参数，附加的I/O操作对象，如果我们没传，源码中会给我们一个它们自己创建的
             */
            @Override
            public void completed(Integer result, ByteBuffer attachment) {
                System.out.println("result = "+result);
                attachment.flip();
                //输出缓冲区中内容
                byte[] data = new byte[attachment.limit()];
                attachment.get(data);
                System.out.println(new String(data));

                attachment.clear();
            }

            /**
             * 执行操作失败调用
             */
            @Override
            public void failed(Throwable exc, ByteBuffer attachment) {

            }
        });
        asynchronousFileChannel.close();
    }

    /**
     * AIO写数据到AIO通道，Future
     */
    @Test
    public void test4() throws Exception{
        //路径
        Path path = Paths.get("E:/1.txt");
        //AIO通道，写
        AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(path, StandardOpenOption.WRITE);
        //缓冲区和位置0
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        long position = 0;
        //放内容到缓冲区
        buffer.put("要写入的内容AIO写数据到AIO通道，Future".getBytes());
        //读写切换
        buffer.flip();
        //写，然后拿到Future
        Future<Integer> operation = asynchronousFileChannel.write(buffer, position);
        buffer.clear();
        //因为AIO是异步的，所以我这里要人为阻塞住它。isDone()方法，直到返回true后，才可以继续运行
        while(!operation.isDone());

        System.out.println("Write over");
    }

    /**
     * AIO写数据到AIO通道，CompletionHandler
     */
    @Test
    public void test5() throws Exception{
        Path path = Paths.get("E:/1.txt");
        if(!Files.exists(path)){
            Files.createFile(path);
        }
        AsynchronousFileChannel asynchronousFileChannel = AsynchronousFileChannel.open(path, StandardOpenOption.WRITE);
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        long position = 0;
        buffer.put("AIO通道通过CompletionHandler写".getBytes());
        buffer.flip();
        asynchronousFileChannel.write(buffer, position, buffer, new CompletionHandler<Integer, ByteBuffer>() {
            @Override
            public void completed(Integer result, ByteBuffer attachment) {
                System.out.println("bytes written:"+result);
            }

            @Override
            public void failed(Throwable exc, ByteBuffer attachment) {
                System.out.println("Write Failed");
                exc.printStackTrace();
            }
        });
        asynchronousFileChannel.close();
    }
}

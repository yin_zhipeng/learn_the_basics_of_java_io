package basics_of_java_io.file;

import java.io.File;

/**
 * 目录操作
 */
public class DirectoryOperationTest {
    public static void main(String[] args) {
        File file = new File("E:/testDirectory/news1.txt");
        if(!file.exists()){//如果路径不存在
            //file.createNewFile();//如果路径不存在，会报错
            //如果目录不存在，并且是一级目录，会创建目录，和文件
            //file.mkdir();//我们这里是两级，这个创建会失败
            file.mkdirs();//如果目录不存在，无论几级目录，都创建多级目录和文件
            System.out.println("\uD83C\uDFC6mkdirs()创建成功："+file.getAbsolutePath());
        }
        System.out.println("\uD83C\uDFC6delete()删除指定文件："+file.delete());


    }
}

package basics_of_java_io.file;

import java.io.*;

/**
 * 文件创建
 */
public class FileCreateTest {
    public static void main(String[] args) {
        FileCreateTest fileCreateTest = new FileCreateTest();
        fileCreateTest.createByPathName("E:\\news1.txt");// 方式一
        fileCreateTest.createByParentChild();//方式二
        fileCreateTest.createByParentChildPath();//方式三

    }

    // 方式一、new File(String pathname)
    public void createByPathName(String pathname){
        File file = new File(pathname);
        try {
            boolean newFile = file.createNewFile();
            System.out.println(pathname+"文件创建完成");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //方式二、new File(File parent,String child) //根据父目录文件+子路径构建
    //E:\\父目录，news2.txt子路径
    public void createByParentChild(){
        File file = new File(new File("E:\\"), "news2.txt");//文件创建在内存

        try {
            boolean newFile = file.createNewFile();//写到磁盘
            System.out.println(file.getAbsolutePath()+"文件创建完成");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //方式三、new File(String parent,String child) //根据父目录路径+子路径构建
    public void createByParentChildPath(){
        File file = new File("E:\\", "news3.txt");
        try {
            boolean newFile = file.createNewFile();
            System.out.println(file.getAbsolutePath()+"文件创建完成");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package basics_of_java_io.file;

import java.io.File;
import java.io.FileInputStream;

/**
 * 文件信息
 */
public class FileInformation {
    public static void main(String[] args) {
        File file = new File("E:/news1.txt");
        System.out.println("\uD83C\uDFC6获取文件名:"+file.getName());
        System.out.println("\uD83C\uDFC6获取文件绝对路径:"+file.getAbsolutePath());
        System.out.println("\uD83C\uDFC6获取当前file的父路径:"+file.getParent());
        System.out.println("\uD83C\uDFC6获取file长度:"+file.length());
        System.out.println("\uD83C\uDFC6文件是否存在:"+file.exists());
        System.out.println("\uD83C\uDFC6是否是文件:"+file.isFile());
        System.out.println("\uD83C\uDFC6是否为目录:"+file.isDirectory());
    }
}

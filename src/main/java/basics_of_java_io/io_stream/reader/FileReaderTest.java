package basics_of_java_io.io_stream.reader;

import java.io.FileReader;
import java.io.IOException;

public class FileReaderTest {
    public static void main(String[] args) {
        try {
            FileReaderTest fileReaderTest = new FileReaderTest();
//            fileReaderTest.test1();
            fileReaderTest.test2();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * read()方法读取单个字符
     */
    public void test1()throws IOException{
        FileReader fileReader = new FileReader("E:/news3.txt");
        int data = 0;//保存单个字符的数据
        while((data = fileReader.read())!=-1){
            System.out.print((char)data);
        }
    }
    //使用字符数组读取
    public void test2()throws IOException{
        FileReader fileReader = new FileReader("E:/news3.txt");
        int dataCount = 0;//保存读取的字符数
        char[] bytes = new char[10];
        while((dataCount = fileReader.read(bytes))!=-1){
            System.out.print(new String(bytes,0,dataCount));
        }
    }
}

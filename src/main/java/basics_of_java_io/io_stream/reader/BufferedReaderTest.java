package basics_of_java_io.io_stream.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * 字符缓冲输入流
 */
public class BufferedReaderTest {
    public static void main(String[] args) {
        BufferedReaderTest bufferedReaderTest = new BufferedReaderTest();
        try {
            bufferedReaderTest.test1();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //BufferedReader处理流封装FileReader节点流
    public void test1() throws IOException {
        System.out.println("=====>>>\uD83C\uDFC6BufferedReader处理流封装FileReader节点流，read()方法读取news4中文本，每次读一个字符\uD83C\uDFC6<<<======");

        BufferedReader bufferedReader = new BufferedReader(new FileReader("E:/news3.txt"));

        int data = 0;//读出字符的二进制，转换成int型
        while((data =  bufferedReader.read())!=-1){
            System.out.print(((char) data));
        }
        bufferedReader.close();//关处理流，会自动关节点流
        System.out.println("=====>>>\uD83C\uDFC6BufferedReader处理流封装FileReader节点流，read(char[],int,int)方法读取news4中文本，每次读一个字符\uD83C\uDFC6<<<======");

        BufferedReader bufferedReader2 = new BufferedReader(new FileReader("E:/news3.txt"));
        char[] chars = new char[10];
        int dataCount = 0;//读出的字符个数
        while((dataCount =  bufferedReader2.read(chars))!=-1){
            System.out.print(new String(chars,0,dataCount));
        }
        bufferedReader2.close();

        System.out.println("==高效===>>>\uD83C\uDFC6readLine()方法读取,每次读一行，遇到换行符就认为一行读完，但是换行符不会读取\uD83C\uDFC6<<<======");

        BufferedReader bufferedReader3 = new BufferedReader(new FileReader("E:/news3.txt"));

        String readLine = "";

        while((readLine = bufferedReader3.readLine())!=null){
            System.out.println(readLine);
        }

        bufferedReader3.close();
    }
}

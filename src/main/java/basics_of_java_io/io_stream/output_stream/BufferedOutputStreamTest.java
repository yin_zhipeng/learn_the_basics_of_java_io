package basics_of_java_io.io_stream.output_stream;

import java.io.*;

public class BufferedOutputStreamTest {
    public static void main(String[] args) {
        BufferedOutputStreamTest bufferedOutputStreamTest = new BufferedOutputStreamTest();
        try {
            bufferedOutputStreamTest.test1();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void test1() throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("E:/jins.jpg"));
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream("E:/copy.jpg"));

        System.out.println("将E:/jins.jpg复制到E:/copy.jpg");
        //读数据
        int dataCount = 0;
        byte[] bytes = new byte[1024];
        while((dataCount = bufferedInputStream.read(bytes))!=-1){
            bufferedOutputStream.write(bytes,0,dataCount);
        }
        bufferedOutputStream.close();
        bufferedInputStream.close();
    }
}

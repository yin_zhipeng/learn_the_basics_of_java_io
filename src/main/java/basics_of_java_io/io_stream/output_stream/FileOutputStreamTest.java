package basics_of_java_io.io_stream.output_stream;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 文件字符输出流FileOutputStream
 */
public class FileOutputStreamTest {
    public static void main(String[] args) {
        FileOutputStreamTest fileOutputStreamTest = new FileOutputStreamTest();
        try {
//            fileOutputStreamTest.test();
//            fileOutputStreamTest.test2();
            fileOutputStreamTest.test3();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //输出内容到news2.txt文件
    public void test() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("E:/news2.txt");

        FileInputStream fileInputStream = new FileInputStream("E:/news1.txt");
        byte[] bytes = new byte[1024];
        System.out.println("从news1.txt文件读取内容:");
        while(fileInputStream.read(bytes)!=-1){
            System.out.print(new String(bytes));
            //输出
            fileOutputStream.write(bytes);

        }
        System.out.println("写入完成");
        fileInputStream.close();
        fileOutputStream.close();
    }
    //追加内容到news2.txt文件
    public void test2() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("E:/news2.txt",true);

        FileInputStream fileInputStream = new FileInputStream("E:/news1.txt");
        byte[] bytes = new byte[1024];
        System.out.println("从news1.txt文件读取内容:");
        while(fileInputStream.read(bytes)!=-1){
            System.out.print(new String(bytes));
            //输出
            fileOutputStream.write(bytes);

        }
        System.out.println("追加完成");
        fileInputStream.close();
        fileOutputStream.close();
    }
    //将news2.txt文件拷贝到testDirectory文件夹下
    public void test3() throws IOException {
        FileInputStream fileInputStream = new FileInputStream("E:/su.jfif");
        FileOutputStream fileOutputStream = new FileOutputStream("E:/testDirectory/su.jfif");

        int byteCount = 0;
        byte[] bytes = new byte[1024];
        while((byteCount = fileInputStream.read(bytes))!=-1){
            fileOutputStream.write(bytes,0,byteCount);//读出多少，就写入多少，否则会写多
        }
        System.out.println("复制E:/su.jfif文件到E:/testDirectory/su.jfif成功");
        if(fileInputStream != null){
            fileInputStream.close();
        }
        if(fileOutputStream !=null){
            fileOutputStream.close();
        }

    }
}

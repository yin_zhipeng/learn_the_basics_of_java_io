package basics_of_java_io.io_stream.objectStream;

import java.io.*;

/**
 * 使用Object*****Stream，完成序列化和反序列化
 */
public class SerializableTest {
    public static void main(String[] args) {
        SerializableTest serializableTest = new SerializableTest();

        try {
//            serializableTest.testObjectOutputStream();
            serializableTest.testObjectInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 将Student对象序列化到student.dat文件
     * @throws IOException
     */
    public void testObjectOutputStream()throws IOException {
        Student student = new Student();
        student.setUsername("张三");
        student.setAge(15);
        String filePath = "E:/Student.dat";//序列化到指定位置文件
        //需要序列化到指定文件，需要传入节点流，否则不需要
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath));

        //因为基本数据类型包装类都实现了Serializable接口，所以都可以直接序列化
        oos.writeInt(10);
        oos.writeBoolean(true);
        oos.writeChar('a');
        oos.writeUTF("字符串");
        //序列化Student对象
        oos.writeObject(student);

        oos.close();
        System.out.println("序列化完成！！！！！！！！！！！！！！！！！！！！！！");
    }
    /**
     * 将student.bat文件，反序列化成java对象
     * @throws IOException
     */
    public void testObjectInputStream() throws IOException{
        String filePath = "E:/Student.dat";//反序列化所需文件路径
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath));
        //读取顺序和保存顺序必须一致
        int read = ois.readInt();
        boolean b = ois.readBoolean();
        char c = ois.readChar();
        String s = ois.readUTF();
        Student student = null;
        try {
            student = (Student)ois.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ois.close();
        System.out.println("反序列化完毕：==>>"+read+"==>>"+b+"==>>"+c+"==>>"+s+"==>>"+student+"==>>");
    }
}

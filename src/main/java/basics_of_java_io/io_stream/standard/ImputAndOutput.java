package basics_of_java_io.io_stream.standard;

/**
 * 标准输入输出流
 */
public class ImputAndOutput {
    public static void main(String[] args) {
        //System.in 是 System 类的  public final static InputStream in = null; 属性
        //System.in 编译类型 InputStream
        //System.in 运行类型为 java.io.BufferedInputStream
        //System.in 标准输入，代表键盘
        //new Scanner(System.in); 就是接收"键盘"输入，所以他是用IO流实现的，需要关掉。scanner.close();
        System.out.println("System.in 运行类型为 :"+System.in.getClass());//标准输入
        //System.out 是 System 类的 public final static PrintStream out = null; 属性
        //System.out 编译类型 PrintStream
        //System.out 运行类型为 java.io.PrintStream
        //System.out 标准输出，代表显示器
        //System.out.println("hello"); 就是输出字符串打印到显示器
        System.out.println("System.out 运行类型为 :"+System.out.getClass());//标准输出


    }

}

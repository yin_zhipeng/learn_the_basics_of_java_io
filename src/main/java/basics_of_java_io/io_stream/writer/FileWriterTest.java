package basics_of_java_io.io_stream.writer;

import java.io.FileWriter;
import java.io.IOException;

public class FileWriterTest {
    public static void main(String[] args) {

        FileWriterTest fileWriterTest = new FileWriterTest();
        try {
            fileWriterTest.test1();
            fileWriterTest.test2();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //字符写
    public void test1() throws IOException {
        FileWriter fileWriter = new FileWriter("E:/news4.txt");
        String str = "打扫宿舍！2！3asdf";
        char[] chars = str.toCharArray();
        for(char c : chars){
            fileWriter.write(c);
        }
        System.out.println("打扫宿舍！2！3asdf=====>>>已写入到E:/news4.txt");
        fileWriter.flush();
        fileWriter.close();//close == flush+关闭

    }
    //字符数组写
    public void test2() throws IOException {
        FileWriter fileWriter = new FileWriter("E:/news4.txt",true);
        String str = "打扫宿舍！2！3asdf";
        char[] chars = str.toCharArray();
        fileWriter.write(chars);

        System.out.println("打扫宿舍！2！3asdf=====>>>已写入到E:/news4.txt");
        fileWriter.flush();
        fileWriter.close();//close == flush+关闭

    }
}

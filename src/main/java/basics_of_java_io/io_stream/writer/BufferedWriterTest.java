package basics_of_java_io.io_stream.writer;

import java.io.*;

/**
 * 字符缓冲输出流
 */
public class BufferedWriterTest {
    public static void main(String[] args) {
        BufferedWriterTest bufferedWriterTest = new BufferedWriterTest();
        try {
            bufferedWriterTest.test();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void test() throws IOException {
        System.out.println("===>>>>write(chars)，将字符串：BufferedWriter缓冲字符输出流 输到E:/news4.txt<<<====");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("E:/news4.txt"));
        String data = "BufferedWriter缓冲字符输入流";
        char[] chars = data.toCharArray();
        bufferedWriter.write(chars);
        bufferedWriter.close();

        System.out.println("===>>>>write(chars)，将news1.txt文件中内容，输出到E:/news4.txt，newLine()方法换行<<<====");
        BufferedReader bufferedReader = new BufferedReader(new FileReader("E:/news1.txt"));
        BufferedWriter bufferedWriter2 = new BufferedWriter(new FileWriter("E:/news4.txt",true));
        bufferedWriter2.newLine();//流中自己输出换行
        int charCount = 0;
        char[] chars1 = new char[5];
        while ((charCount=bufferedReader.read(chars1))!=-1){
            bufferedWriter2.write(chars1,0,charCount);
        }
        bufferedWriter2.close();
    }

}

package basics_of_java_io.io_stream.transformation;

import java.io.*;

/**
 * 转换流，OutputStreamWriter
 */
public class OutputStreamWriterTest {
    public static void main(String[] args) {
        OutputStreamWriterTest outputStreamWriterTest = new OutputStreamWriterTest();
        try {
            outputStreamWriterTest.test1();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void test1()throws IOException {
        //转换流
        InputStreamReader isr = new InputStreamReader(new FileInputStream("E:/news3.txt"),"UTF-8");
        OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream("E:/news4.txt"), "utf-8");
        //使用缓冲流，提高效率
        BufferedWriter bw = new BufferedWriter(osw);
        BufferedReader br = new BufferedReader(isr);
        //写入
        int dataCount = 0;
        char[] chars = new char[10];
        while((dataCount = br.read(chars))!=-1){
            System.out.print(new String(chars,0,dataCount));
            bw.write(chars,0,dataCount);
        }
        br.close();//关闭外层流，会自动关闭里层流
        bw.close();
        System.out.println("用utf-8编码写入完成");
    }
}

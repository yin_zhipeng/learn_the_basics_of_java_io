package basics_of_java_io.io_stream.transformation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 转换流，InputStreamReader
 */
public class InputStreamReaderTest {
    public static void main(String[] args) {
        InputStreamReaderTest inputStreamReaderTest = new InputStreamReaderTest();
        try {
            inputStreamReaderTest.test1();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void test1() throws IOException{
        //转换流
        InputStreamReader isr = new InputStreamReader(new FileInputStream("E:/news3.txt"),"UTF-8");
        //使用缓冲流，提高效率
        BufferedReader br = new BufferedReader(isr);
        //读取
        int dataCount = 0;
        char[] chars = new char[10];
        while((dataCount = br.read(chars))!=-1){
            System.out.print(new String(chars,0,dataCount));
        }
        br.close();//关闭外层流，会自动关闭里层流
//        isr.close();
    }
}

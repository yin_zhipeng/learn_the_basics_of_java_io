package basics_of_java_io.io_stream.print;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 字符打印流
 */
public class PrintWriterTest {
    public static void main(String[] args) {
        PrintWriterTest printWriterTest = new PrintWriterTest();
        try {
            printWriterTest.test1();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void test1()throws IOException{
        //打印到控制台
        PrintWriter systemOut = new PrintWriter(System.out);
        systemOut.print("指定输出流为标准输出流System.out，输出");
        //打印到文件
        PrintWriter pw = new PrintWriter(new FileWriter("E:/new5.txt", true));
        pw.println("指定输出流为文件输出流new FileWriter，输出");
        systemOut.close();
        pw.close();
    }
}


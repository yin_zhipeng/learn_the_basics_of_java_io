package basics_of_java_io.io_stream.print;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

/**
 * 字节打印流
 */
public class PrintStreamTest {
    public static void main(String[] args) {
        PrintStreamTest printStreamTest = new PrintStreamTest();
        try {
            printStreamTest.test1();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void test1() throws IOException{
        //字节打印流
        PrintStream out = System.out;
        //输出信息到控制台
        out.println("通过PrintStream输出信息到控制台");
        out.write("println底层调用write完成功能，所以调用write()效果一样".getBytes(StandardCharsets.UTF_8));
        //修改打印流，打印位置，为new5.txt文件
        System.setOut(new PrintStream("E:/new5.txt"));
        System.out.println("System.setOut(new PrintStream(\"E:/new5.txt\"));将打印位置设置为new5.txt");
        out.flush();
        out.close();
    }
}

package basics_of_java_io.io_stream.input_stream;


import java.io.FileInputStream;
import java.io.IOException;

/**
 * 文件字符输入流FileInputStream
 */
public class FileInputStreamTest {
    public static void main(String[] args) {
        FileInputStreamTest fileInputStreamTest = new FileInputStreamTest();
        try {
            System.out.println("============================使用read()方法读==============================");
            fileInputStreamTest.readTest();
            System.out.println("============================使用read(byte)方法读==============================");
            fileInputStreamTest.readByByteTest();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //使用read()方法读，从输入流读取一个字节（保存在int型变量中），没有输入可用，此方法将阻止
    //如果返回-1，表示读取完毕
    public void readTest() throws IOException {
        String filePath = "E:/news1.txt";
        FileInputStream fileInputStream = new FileInputStream(filePath);//创建FileInputStream对象，用于读取文件
        int readData = 0;//保存read()方法每次读出的字节
        while((readData = fileInputStream.read())!=-1){
            System.out.print((char)readData);//这种形式中文会乱码，因为中文是3个字节，char类型2个字节，强转后，会丢二进制位
        }
        System.out.println();
        //关闭流
        fileInputStream.close();
    }
    //使用read(byte[])方法读，读出btye.length个字节，放到byte中，没有输入可用，此方法将阻止
    //如果返回-1，表示读取完毕。否则返回实际读取到的字节数
    public void readByByteTest() throws IOException {
        String filePath = "E:/news1.txt";
        FileInputStream fileInputStream = new FileInputStream(filePath);//创建FileInputStream对象，用于读取文件
        int byteCount = 0;//用来保存read(bytes)每次读出的字节数
        byte[] bytes = new byte[1024];
        while((byteCount=fileInputStream.read(bytes))!=-1){
            System.out.println("当前数组实际读出的字节数："+byteCount);
            System.out.print(new String(bytes));
        }
        System.out.println();
        //关闭流
        fileInputStream.close();
    }
}

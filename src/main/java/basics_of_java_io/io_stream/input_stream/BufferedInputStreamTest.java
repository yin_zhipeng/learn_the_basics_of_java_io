package basics_of_java_io.io_stream.input_stream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class BufferedInputStreamTest {
    public static void main(String[] args) {
        BufferedInputStreamTest bufferedInputStreamTest = new BufferedInputStreamTest();
        try {
            bufferedInputStreamTest.test();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void test() throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream("E:/news2.txt"));

        int dataCount = 0;
        byte[] bytes = new byte[10];
        while ((dataCount = bufferedInputStream.read(bytes))!=-1){
            System.out.print(new String(bytes,0,dataCount));
        }
    }
}

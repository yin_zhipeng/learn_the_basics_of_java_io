package basics_of_java_io.case_properties;

import java.io.*;
import java.net.URL;
import java.util.Properties;

/**
 * 读取mysql.properties文件配置信息
 */
public class ReadProperties {
    public static void main(String[] args) {
        ReadProperties readProperties = new ReadProperties();
        try {
            readProperties.readTradtion();
            readProperties.readProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 传统方法
     * @throws IOException
     */
    public void readTradtion()throws IOException {
        InputStream is = ReadProperties.class.getClassLoader().getResourceAsStream("mysql.properties");
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = "";
        while((line = br.readLine())!=null){
            String[] split = line.split("=");
            System.out.println(split[0]+"========================>>>>>>"+split[1]);
        }

        br.close();
    }
    /**
     * Properties类
     * @throws IOException
     */
    public void readProperties()throws IOException {
        URL resource = ReadProperties.class.getClassLoader().getResource("mysql.properties");
        FileReader fileReader = new FileReader(resource.getFile());

        Properties properties = new Properties();
        //加载配置文件
        properties.load(fileReader);
        //读取ip的值
        String ip = properties.getProperty("ip");
        System.out.println("通过Properties类读取ip==================>>>>"+ip);

        fileReader.close();
    }
}
